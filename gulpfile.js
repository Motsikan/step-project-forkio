import gulp from "gulp";
const { series, parallel, src, dest, watch } = gulp;

import browserSync from "browser-sync";
// const browserSync = require('browser-sync').create();
const bsServer = browserSync.create();

import dartSass from "sass";
import gulpSass from "gulp-sass";
const sass = gulpSass(dartSass);
// const sass = require('gulp-sass')(require('sass'));

import imagemin from "gulp-imagemin";
// const imagemin = require("gulp-imagemin");

import minify from "gulp-minify";
// const minify = require('gulp-minify');

import uglify from "gulp-uglify";
// const uglify = require("gulp-uglify");

import concat from "gulp-concat";
// const concat = require("gulp-concat");

import autoprefixer from "gulp-autoprefixer";
// const autoprefixer = require('gulp-autoprefixer');

import cleanCSS from "gulp-clean-css";
// const cleanCSS = require('gulp-clean-css');

import clean from "gulp-clean";
// const clean = require('gulp-clean');

import rename from "gulp-rename";

import minifyjs from "gulp-js-minify";

// Запуск сервера  ----------------------------------------
// --------------------------------------------------------
const serve = () => {
  bsServer.init({
    server: {
      baseDir: "./",
      browser: "firefox.exe",
    },
  });
}

// Очищення папки dist ------------------------------------
// --------------------------------------------------------
const cleaner = () => {
  return src("./dist/*", { read: false }).pipe(clean());
}

// --------------------------------------------------------
// const htmls = () =>  {
//   return src("./*.html")
//     .pipe(
//       fileinclude({
//         prefix: "@@",
//       })
//     )
//     .pipe(dest("./dist/"))
//     .pipe(bsServer.reload({ stream: true }));
// }

// Mінімізація зображень та копіювання їх у папку dist ----------------
// --------------------------------------------------------------------
const images = () => {
  return src("./src/img/**/*.{jpg,jpeg,png,svg,webp}")
    .pipe(imagemin())
    .pipe(dest("./dist/img/"))
    .pipe(bsServer.reload({ stream: true }));
}

// Компіляція файлів scss в css ---------------------------- 
// ---------------------------------------------------------
const styles = () => {
  return (
    src("./src/scss/styles.scss")
      .pipe(sass().on("error", sass.logError))
      .pipe(
        autoprefixer(["last 15 versions", ">1%", "ie 8", "ie 7"], {
          cascade: true,
        })
      )
      .pipe(dest("./dist/css/"))
      .pipe(cleanCSS())
      .pipe(
        rename({
          extname: ".min.css",
        })
      )
      .pipe(dest("./dist/css/"))
      .pipe(bsServer.reload({ stream: true }))
  );
}

// Конкатенація та мініфікація js --------------------------
// ---------------------------------------------------------
const scripts = () => {
  return (
    src("./src/js/**/*.js")
      .pipe(dest("./dist/js/"))
      .pipe(concat("script.min.js"))
      .pipe(minifyjs())
      //  .pipe(
      //    rename({
      //      extname: ".min.js",
      //    })
      //  )
      .pipe(dest("./dist/js/"))
      .pipe(bsServer.reload({ stream: true }))
  );
}

// Відстеження змін ----------------------------------------
// ---------------------------------------------------------
const watcher = () => {
  watch("./src/img/**/*.{jpg,jpeg,png,svg,webp}", images);
  watch("./src/scss/*.scss", styles);
  watch("./src/js/script.js", scripts);
  watch("./index.html").on("change", bsServer.reload);
}

// Pобоче завдання build -----------------------------------
// --------------------------------------------------------- 
gulp.task("build", gulp.series(cleaner, images, styles, scripts));

// Pобоче завдання dev -------------------------------------
// ---------------------------------------------------------
gulp.task("dev", gulp.series("build", gulp.parallel(serve, watcher)));

